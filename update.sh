#!/bin/bash

KEY=sgtools@bmel.fr

cd public

echo "Create Packages"
dpkg-scanpackages --multiversion --arch amd64 pool/ > dists/jammy/main/binary-amd64/Packages

echo "Create Packages.gz"
cat dists/jammy/main/binary-amd64/Packages | gzip -9 > dists/jammy/main/binary-amd64/Packages.gz

echo "Create Release"
apt-ftparchive release dists/jammy > dists/jammy/Release
sed -i "1iSuite: jammy" dists/jammy/Release
sed -i "2iVersion: 22.04" dists/jammy/Release
sed -i "3iCodename: jammy" dists/jammy/Release
sed -i "5iArchitectures: amd64" dists/jammy/Release
sed -i "6iComponents: main" dists/jammy/Release
sed -i "7iDescription: Ubuntu Jammy 22.04" dists/jammy/Release

echo "Create Release.gpg"
cat dists/jammy/Release | gpg --batch --yes --default-key $KEY -abs > dists/jammy/Release.gpg

echo "Create InRelease"
cat dists/jammy/Release | gpg --batch --yes --default-key $KEY -abs --clearsign > dists/jammy/InRelease

