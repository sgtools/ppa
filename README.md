A PPA repository for [SGTools](https://gitlab.com/sgtools) packages

- [cmd](https://gitlab.com/sgtools/cmd)
- [bump-version](https://gitlab.com/sgtools/bump-version)
- [csv2ofx](https://gitlab.com/sgtools/csv2ofx)
- [rpass](https://gitlab.com/sgtools/rpass)
- [rcode](https://gitlab.com/sgtools/rcode)
- [rdocufy](https://gitlab.com/sgtools/rdocufy)

# Usage (Ubuntu Jammy 22.04)

```bash
sudo sh -c 'curl -SsL https://sgtools.gitlab.io/ppa/pgp-key.public | gpg --dearmor > /etc/apt/trusted.gpg.d/sgtools.gpg'
sudo sh -c 'curl -SsL -o /etc/apt/sources.list.d/sgtools.list https://sgtools.gitlab.io/ppa/sgtools.list'
sudo apt update
sudo apt install cmd
sudo apt install bump-version
sudo apt install csv2ofx
sudo apt install rpass
sudo apt install rcode
sudo apt install rdocufy
```

